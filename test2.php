<?php
// Реализовать алгоритм который будет работать с массивом
// любой вложенности и с любым набором данных и ключей. 
// Алгоритм должен обойти весь массив и все под массивы,
// найти отобразить следующую информацию:
// - Самое большое целое число в массиве
// - Самое минимальное целое число в массиве
// - Чаще всего встречающееся целое число в массиве (если такого нет,
// то отобразить среднее значение все целых чисел в массиве)
// - Найти самую длинную строку
// - Найти самую короткую строку


$arr = array(
    'first' => array("iPhone 12 pro max", 1999, "iPhone 12 Pro"),
    'next' => array(
        'last' => array(
            'value'=> 123,
            'value2' => 'TXT',
            'value22' => 1333,
            'val33333' => 11111,
        )
        ), 'e', 10
);

function arrayPush($innerArr)
{   
    // Объявляем статическую переменную для применения в рекурсии
    static $result;

    foreach ($innerArr as $value)
    {
        if(gettype($value) == "string")
        {
            if (!isset($result['MIN_LENGHT']))
            $result['MIN_LENGHT'] = $value;
            elseif (strlen($value) < strlen($result['MIN_LENGHT']))
            {
                $result['MIN_LENGHT'] = $value;
            }
            if (strlen($value) > strlen($result['MAX_LENGHT']))
            {
                $result['MAX_LENGHT'] = $value;
            }
        }
        elseif (gettype($value) == "integer")
        {   
            if ($value > $result['MAX_INT'])
            {
                $result['MAX_INT'] = $value;
            }
            if (!isset($result['MIN_INT']))
            $result['MIN_INT'] = $value;
            elseif ($value < $result['MIN_INT'])
            {
                $result['MIN_INT'] = $value;
            }
        } 
        elseif (gettype($value) == 'array')
        {
            arrayPush($value);
        }
    }
    // Возвращаем результирующий массив
    return $result;
}
$res = arrayPush($arr);
echo 'Максимально длинная строка: '.$res['MAX_LENGHT'].'</br>';
echo 'Минимально длинная строка: '.$res['MIN_LENGHT'].'</br>';
echo 'Максимальное число: '.$res['MAX_INT'].'</br>';
echo 'Минимальное число: '.$res['MIN_INT'].'</br>';
