<?php

// Написать класс для работы с текстом который должен уметь следующее:
// 
// 1) Генерировать уникальную строку которую генератор никогда
// не сможет повторить при повторной генерации.
// 2) Переводить текст в транслит
// 3) Разбивать текст на массив слов
// 4) Считать количество слов в строке
// 5) Считать количество символов в строке не считая пробелов
// 6) Убирать из строки все лишние пробелы || Не понял задания. Есть trim)
// 7) Искать есть ли в строке все слова из входного массива слов

class Textreader
{   
    // 1) генератор строки
    public function generateString($lenght)
    {
        $chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $input_length = strlen($chars);
        $random_string = '';
        for($i = 0; $i < $lenght; $i++)
        {
            $random_character = $chars[mt_rand(0, $input_length - 1)];
            $random_string .= $random_character;
        }
 
    return $random_string;
    }
    // 2) Переводить текст в транслит
    public function getTranslate($text)
    {
        $converterArray = array(
            'а' => 'a',    'б' => 'b',    'в' => 'v',    'г' => 'g',    'д' => 'd',
            'е' => 'e',    'ё' => 'e',    'ж' => 'zh',   'з' => 'z',    'и' => 'i',
            'й' => 'y',    'к' => 'k',    'л' => 'l',    'м' => 'm',    'н' => 'n',
            'о' => 'o',    'п' => 'p',    'р' => 'r',    'с' => 's',    'т' => 't',
            'у' => 'u',    'ф' => 'f',    'х' => 'h',    'ц' => 'c',    'ч' => 'ch',
            'ш' => 'sh',   'щ' => 'sch',  'ь' => '',     'ы' => 'y',    'ъ' => '',
            'э' => 'e',    'ю' => 'yu',   'я' => 'ya',
     
            'А' => 'A',    'Б' => 'B',    'В' => 'V',    'Г' => 'G',    'Д' => 'D',
            'Е' => 'E',    'Ё' => 'E',    'Ж' => 'Zh',   'З' => 'Z',    'И' => 'I',
            'Й' => 'Y',    'К' => 'K',    'Л' => 'L',    'М' => 'M',    'Н' => 'N',
            'О' => 'O',    'П' => 'P',    'Р' => 'R',    'С' => 'S',    'Т' => 'T',
            'У' => 'U',    'Ф' => 'F',    'Х' => 'H',    'Ц' => 'C',    'Ч' => 'Ch',
            'Ш' => 'Sh',   'Щ' => 'Sch',  'Ь' => '',     'Ы' => 'Y',    'Ъ' => '',
            'Э' => 'E',    'Ю' => 'Yu',   'Я' => 'Ya',
        );
     
        $result = strtr($text, $converterArray);
        return $result;
    }
    // 3) Разбивать текст на массив слов
    public function getArrayWords($text)
    {
        $tokens = " \t\n";
        $tok = strtok($text, $tokens);
        while($tok)
        {
            $arrayText[] = $tok;
            $tok = strtok($tokens);
        }
        return $arrayText;
    }
    // 4) Считать количество слов в строке
    public function getCountWords($text)
    {
        $count = count($this->getArrayWords($text));
        return $count;
    }
    // 5) Считать количество символов в строке не считая пробелов
    public function getCountChars($string)
    {
        // "Бьем" слова на массивы, тем самым убираем пробелы
        $arr = explode(" ", $string);
        // Считаем кол-во массивов для необходимого кол-ва итераций цикла по подсчету
        // общего кол-ва символов в элементах массива
        $count = count($arr);
        echo $count.'<br>';
        $i = 0;
        $result = 0;
        while ($i !== $count)
        {   
            $result+= strlen($arr[$i]);
            $i++;
        }
        return $result;
    }
    //  7) Искать есть ли в строке все слова из входного массива слов
    public function getSearch($arr, $string)
    {
        foreach ($arr as $value)
        {
            if(stristr($string, $value) === FALSE) {
                echo "Слово \"".$value."\" не найдено в строке<br>";
              }
            else
            {
                echo "Слово \"",$value."\" есть в строке<br>";
            }
        }
        
    }
}