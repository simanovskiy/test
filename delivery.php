<?php
interface Delivery {
    public function getCost($weight);
}
class Courier implements Delivery{
    // Порог веса фиксированной стоимости
    public static $weightFix;
    // Фиксированая цена
    public static $costFix;
    // Второе ограничение по весу
    public static $stepWeight;
    // Цена за кг в первой ступени
    public static $firstStepCost;
    // Цена за кг свыше 
    public static $secondStepCost; 
    
    public function __construct( $weightFix, $costFix, $stepWeight, $firstStepCost, $secondStepCost)
    {
        self::$weightFix = $weightFix;
        self::$costFix = $costFix;
        self::$stepWeight = $stepWeight;
        self::$firstStepCost = $firstStepCost;
        self::$secondStepCost = $secondStepCost;
    }
    public function getCost($weight)
    {
        $weight = ceil($weight);
        if ($weight < self::$weightFix)
        {
            return self::$costFix;
        }
        elseif (($weight > self::$weightFix) and ($weight < self::$stepWeight))
        {
            $result = $weight * self::$firstStepCost;
            return $result;
        }
        elseif ($weight > self::$stepWeight)
        {
            $result = $weight * self::$secondStepCost;
            return $result;
        }
    }
}

// Почта
class Post implements Delivery {
    // Ограничение по весу для малой цены
    public static $weightLimit;
    // Минимальная фикс цена
    public static $minCost;
    // Максимальная цена
    public static $maxCost;
    public function __construct($weightLimit, $minCost, $maxCost)
    {
        self::$weightLimit = $weightLimit;
        self::$minCost = $minCost;
        self::$maxCost = $maxCost;
    }
    public function getCost($weight)
    {   
        $weight = ceil($weight);
        if ($weight <= self::$weightLimit)
        {
            return self::$minCost;
        }
        else
        {
            return self::$maxCost;
        }
    }
}

class StepDelivery implements Delivery {
    // Базовая цена за кг
    public $result;
    public static $baseCost;
    // Прибавние стоимости с каждого шага
    public static $stepCost;
    // Шаг веса
    public static $stepWeight;
    public function __construct($baseCost, $stepCost, $stepWeight)
    {
        self::$baseCost = $baseCost;
        self::$stepCost = $stepCost;
        self::$stepWeight = $stepWeight;
    }
    public function getCost($weight){
        $result = 0;
        // Фиксируем базовую цену за кг
        $costPerKg = self::$baseCost;
        // Сразу округляем вес в большую сторону
        $weight = ceil($weight);
        // Определяем кол-во десятков
        $step = floor($weight / self::$stepWeight);
        // "Остаток" от десятков
        $balance = $weight % self::$stepWeight;
        
        if ($step > 0){
            $i = 0;
            do {
                $result = $result + ($costPerKg * self::$stepWeight);
                $i++;
                $costPerKg = $costPerKg + self::$stepCost;
            } while ($i < $step);
        }
        // Досчитываем остаток кг по актуальной цене
        if (isset($balance))
        { 
            $result = $result + ($balance * $costPerKg);

        }
        return $result;
    }
}



